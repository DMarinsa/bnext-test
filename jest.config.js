module.exports = {
    globals: {
        'ts-jest': {
            tsconfig: 'tsconfig.json',
        },
    },
    moduleFileExtensions: [
        'ts',
        'js',
    ],
    testEnvironment: 'node',
    testMatch: [
        '**/test/**/*.test.(ts|js)',
    ],
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
};
