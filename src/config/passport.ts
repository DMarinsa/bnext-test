import passport from 'passport';
import passportLocal from 'passport-local';
import _ from 'lodash';

// import { User, UserType } from '../models/User';
import { User, UserDocument, } from '../models/User';
import { Request, Response, NextFunction, } from 'express';

const LocalStrategy = passportLocal.Strategy;

passport.serializeUser<any, any>((user, done) => {
    done(undefined, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});


passport.use(new LocalStrategy({ usernameField: 'phone', }, async (phone, password, done) => {
    User.findOne({ phone, }, (err, user: any) => {
        if (err) { return done(err); }
        if (!user) {
            return done(undefined, false, { message: `phone ${phone} not found.`, });
        }
        user.comparePassword(password, (err: Error, isMatch: boolean) => {
            if (err) { return done(err); }
            if (isMatch) {
                return done(undefined, user);
            }
            return done(undefined, false, { message: 'Invalid phone or password.', });
        });
    });
}));

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated()) {
        return next();
    }
    throw Error('Authorization Error');
};
