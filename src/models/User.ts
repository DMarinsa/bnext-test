import bcrypt from 'bcrypt-nodejs';
import mongoose, { Schema, } from 'mongoose';

export type UserDocument = mongoose.Document & {
  _id: mongoose.Types.ObjectId;
  country: string;
  metadata: any;
  name: string;
  password: string;
  phone: number;
  socialNumberId: string;
  comparePassword: comparePasswordFunction;
};

type comparePasswordFunction = (candidatePassword: string, cb: (err: any, isMatch: any) => {}) => void;

export interface AuthToken {
    accessToken: string;
    kind: string;
}

const userSchema = new mongoose.Schema({
  country: {
    required: true,
    type: String,
  },
  metadata: {
    type: Schema.Types.Mixed,
  },
  name: {
    required: true,
    type: String,
  },
  password: {
    required: true,
    type: String,
  },
  phone: {
    index: true,
    required: true,
    type: Number,
    unique: true,
  },
  socialNumberId: {
    required: true,
    type: String,
  },
}, { timestamps: true, });

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
    const user = this as UserDocument;
    if (!user.isModified('password')) { return next(); }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) { return next(err); }
        bcrypt.hash(user.password, salt, undefined, (err: mongoose.Error, hash) => {
            if (err) { return next(err); }
            user.password = hash;
            next();
        });
    });
});

const comparePassword: comparePasswordFunction = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err: mongoose.Error, isMatch: boolean) => {
        cb(err, isMatch);
    });
};

userSchema.methods.comparePassword = comparePassword;

export const User = mongoose.model<UserDocument>('User', userSchema);
