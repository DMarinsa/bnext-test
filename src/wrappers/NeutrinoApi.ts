
import axios, { AxiosResponse, } from 'axios';
const HOST_URL = 'https://neutrinoapi.net';
export class NeutrinoApi {
  constructor (private userId: string, private apiKey: string) {}

  async postHrlLookup(phone: number, countryCode: string): Promise<AxiosResponse> {
    const hrlLookUpUrl = HOST_URL + '/hlr-lookup';

    const data = {
      'api-key': this.apiKey,
      'country-code': countryCode,
      'number': phone,
      'user-id': this.userId,
    };
    return axios.post(hrlLookUpUrl, data);
  }
}
