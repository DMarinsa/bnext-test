
import { Contact, ContactDocument, } from '../models/Contact';
import { User, UserDocument, } from '../models/User';
import { Request, Response, } from 'express';

import { check, validationResult, } from 'express-validator';
import '../config/passport';

import * as _ from 'lodash';


export const postContacts = async (req: Request, res: Response) => {

  await check('arr.*phone', 'Phone is not valid')
  .isMobilePhone('any')
  .run(req);
  await check('arr.*contactName', 'ContactName cannot be blank')
  .isAlphanumeric()
  .run(req);

  const errors = validationResult(req.body);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.status(400).json(errors.array());
  }

  const currentUser = req.user as UserDocument;
  const userPhones = req.body.map((element: { phone: number }) => element.phone);
  const foundUsers = await User.find({
    phone: {
    '$in': userPhones,
    },
  })
  .lean()
  .exec();

  const responseErrors: Array<Error> = [];
  const mappedContacts = req.body.map((element: { contactName: string; phone: number }) => {
    const user = foundUsers.find((user: UserDocument) => user.phone === element.phone);
    if (!user) {
      responseErrors.push(Error('USER_NOT_EXISTS'));
      return;
    }

    if (currentUser.phone === element.phone) {
      responseErrors.push(Error('CANNOT_ADD_YOURSELF_AS_A_CONTACT'));
      return;
    }

    return {
      contactId: user._id,
      contactName: element.contactName,
      contactPhone: element.phone,
      userId: currentUser._id,
    };
  });

  const response: {
    errors: Array<{ msg: string }>;
    contacts?: Array<ContactDocument>;
  } = {
    errors: responseErrors.map(error => { 
      return {
        msg: error.toString(),
      };
    }),
  };
  if (mappedContacts.length) {
    const insertedContacts = await Contact.insertMany(_.compact(mappedContacts));
    response.contacts = insertedContacts;
  }
  return res.status(200).json(response);
};

export const getContacts = async (req: Request, res: Response) => {

  await check('commonContactsWithId', 'Id is not valid')
  .isMongoId()
  .optional()
  .run(req);

  const errors = validationResult(req.query);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.status(400).json(errors.array());
  }


  const currentUser = req.user as UserDocument;

  let result: Array<ContactDocument> = [];

  const commonContactsWithId = req.query.commonContactsWithId;
  if (commonContactsWithId) {
    const contacts = await Contact.find({
      userId: {
        '$in': [ commonContactsWithId, currentUser._id, ],
      },
    })
    .lean()
    .exec();

    const userContacts = contacts.filter((contact: ContactDocument) => contact.userId.equals(currentUser._id));
    const relatedContacts = contacts.filter((contact: ContactDocument) => contact.userId.equals(commonContactsWithId));

    result = userContacts.filter((contact: ContactDocument) => {
      const foundCoincidence = relatedContacts.find((relatedContact: ContactDocument) => relatedContact.contactPhone === contact.contactPhone);
      return (foundCoincidence) ? true : false;
    });

  } else {
    result = await Contact.find({
      userId: currentUser._id,
    });
  }

  return res.status(200).json(result);
};

export const deleteContact = async (req: Request, res: Response) => {

  await check('contactId', 'Id is not valid')
  .isMongoId()
  .run(req);

  const errors = validationResult(req.params);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.status(400).json(errors.array());
  }
  
  const currentUser = req.user as UserDocument;
  await Contact.deleteOne({
    _id: req.params.contactId,
    userId: currentUser._id,
  });

  return res.status(200).end();
};


