import bluebird from 'bluebird';
import express from 'express';
import compression from 'compression';  // compresses requests
import session from 'express-session';
import bodyParser from 'body-parser';
import lusca from 'lusca';
import mongo from 'connect-mongo';
import mongoose from 'mongoose';
import flash from 'express-flash';
import path from 'path';
import passport from 'passport';
import { MONGODB_URI, SESSION_SECRET, } from './util/secrets';

const MongoStore = mongo(session);

// Controllers (route handlers)
import * as userController from './controllers/user';
import * as contactController from './controllers/contacts';

// API keys and Passport configuration
import * as passportConfig from './config/passport';

// Create Express server
const app = express();

// connect to db

mongoose.Promise = bluebird;

const dbConnectionOptions = {
  connectTimeoutMS: 10000,
  reconnectInterval: 500,
  reconnectTries: Number.MAX_VALUE,
  useNewUrlParser: true,
};

mongoose.connect('mongodb://mongodb:27017/bnext', dbConnectionOptions);

const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error);
});

database.on('connected', () => {
    console.log('DB CONNECTED');
});

// Express configuration
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, }));
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    store: new MongoStore({
        autoReconnect: true,
        url: MONGODB_URI,
    }),
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.use((req, res, next) => {
    res.locals.user = req.user;
    next();
});
app.use((req, res, next) => {
    // After successful login, redirect back to the intended page
    if (!req.user &&
    req.path !== '/login' &&
    req.path !== '/signup' &&
    !req.path.match(/^\/auth/) &&
    !req.path.match(/\./)) {
        req.session.returnTo = req.path;
    } else if (req.user &&
    req.path == '/account') {
        req.session.returnTo = req.path;
    }
    next();
});

app.use(
    express.static(path.join(__dirname, 'public'), { maxAge: 31557600000, })
);

app.post('/login', userController.postLogin);
app.post('/logout', passportConfig.isAuthenticated, userController.logout);
app.post('/signup', userController.postSignup);
app.post('/contacts', passportConfig.isAuthenticated, contactController.postContacts);
app.get('/contacts', passportConfig.isAuthenticated, contactController.getContacts);
app.delete('/contacts/:contactId', passportConfig.isAuthenticated, contactController.deleteContact);

export default app;
