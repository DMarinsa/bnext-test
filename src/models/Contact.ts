
import mongoose from 'mongoose';

export type ContactDocument = mongoose.Document & {
  contactId: mongoose.Types.ObjectId;
  contactName: string;
  contactPhone: string;
  userId: mongoose.Types.ObjectId;
};

const contactSchema = new mongoose.Schema({
  contactId: {
    indexed: true,
    required: true,
    type: mongoose.Types.ObjectId,
  },
  contactName: {
    required: true,
    type: String,
  },
  contactPhone: {
    required: true,
    type: Number,
  },
  userId: {
    indexed: true,
    required: true,
    type: mongoose.Types.ObjectId,
  },
}, { timestamps: true, });

export const Contact = mongoose.model<ContactDocument>('Contact', contactSchema);
