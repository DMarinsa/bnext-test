FROM node:10.23 AS base

ENV USERNAME=node
ARG USER_ID
ARG GROUP_ID
RUN usermod -u ${USER_ID} ${USERNAME}
RUN usermod -g ${GROUP_ID} ${USERNAME}
ENV WORKPATH=/usr/src/app
RUN mkdir -p ${WORKPATH}/node_modules && chown -R ${USERNAME}:${USERNAME} ${WORKPATH}
WORKDIR ${WORKPATH}

COPY . /usr/src/app
RUN npm install && npm run build

USER ${USERNAME}
FROM base AS dev
EXPOSE 3000
COPY --chown=${USERNAME}:${USERNAME} . .
CMD ["npm", "start"]


