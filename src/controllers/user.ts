import passport from 'passport';
import { User, UserDocument, } from '../models/User';
import { Request, Response, NextFunction, } from 'express';
import { IVerifyOptions, } from 'passport-local';
import { check, validationResult, } from 'express-validator';
import '../config/passport';
import { NeutrinoApi, } from '../wrappers/NeutrinoApi';
import { AxiosResponse, } from 'axios';

const neutrinoApiWrapper = new NeutrinoApi(
  process.env.NEUTRINO_USER,
  process.env.NEUTRINO_API_KEY,
);

/**
 * Sign in using email and password.
 * @route POST /login
 */
export const postLogin = async (req: Request, res: Response, next: NextFunction) => {
    await check('phone', 'Phone is not valid')
      .isMobilePhone('any')
      .run(req);
    await check('password', 'Password cannot be blank')
      .isLength({min: 4, })
      .isAlphanumeric()
      .run(req);

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        req.flash('errors', errors.array());
        return res.status(400).json(errors.array());
    }

    passport.authenticate('local', (err: Error, user: UserDocument, info: IVerifyOptions) => {
        if (err) { return next(err); }
        if (!user) {
            req.flash('errors', {msg: info.message, });
            return res.status(401).json(errors.array());
        }
        req.logIn(user, (err) => {
            if (err) { return next(err); }
            req.flash('success', { msg: 'Success! You are logged in.', });
            return res.status(200).json(user);
        });
    })(req, res, next);
};
export const logout = (req: Request, res: Response) => {
    req.logout();
    return res.status(200).end();
};

async function retrieveMobilePhoneMetadata(phone: number, country: string): Promise<AxiosResponse> {
  const metadata = await neutrinoApiWrapper.postHrlLookup(phone, country);
  const hlrValid = metadata.data['hlr-valid'];
  if (!hlrValid) {
    throw new Error('INVALID_PHONE');
  }
  return metadata;
}

export const postSignup = async (req: Request, res: Response, next: NextFunction) => {
  
  await check('country', 'Country is not valid')
    .isAlpha()
    .isString()
    .isLength({
      max: 2,
      min: 2,
    })
    .run(req);
  await check('phone', 'Phone is not valid')
    .isMobilePhone('any')
    .run(req);
  await check('password', 'Password must be at least 4 characters long').isLength({ min: 4, }).run(req);
  await check('confirmPassword', 'Passwords do not match').equals(req.body.password).run(req);
  await check('name', 'Username is not valid')
    .isString()
    .isLength({
      min: 1,
    })
    .run(req);
  await check('socialNumberId', 'SocialNumberId is not valid')
    .isString()
    .run(req);

  const errors = validationResult(req.body);

  if (!errors.isEmpty()) {
      req.flash('errors', errors.array());
      return res.status(400).json(errors);
  }

  let metadata;
  try {
    metadata = await retrieveMobilePhoneMetadata(req.body.phone, req.body.country);
  } catch (err) {
    const error =  { msg: err, };
    req.flash('errors', error);
    return res.status(400).json(error);
  }

  const user = new User({
    country: req.body.country,
    metadata,
    name: req.body.name,
    password: req.body.password,
    phone: req.body.phone,
    socialNumberId: req.body.socialNumberId,
  });

  const foundUser = await User.findOne({ phone: req.body.phone, });

  if (foundUser) {
    const error =  { msg: 'Account with that phone already exists.', };
    req.flash('errors', error);
    return res.status(400).json(error);
  }

  user.save((err) => {
    if (err) { return next(err); }
    req.logIn(user, (err) => {
        if (err) {
            return next(err);
        }
        return res.status(200).json(user);
    });
  });
};
